import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';


@Injectable()
export class DatabaseInterfaceProvider {
  /* LOCAL DATABASE */
  db: SQLiteObject = null;

  /* SERVER DATABASE */
  // url: string = 'https://espacio.me/api/store';
  // apikey = '7207937a2a2b3d9b0372';

  url: string = '';
  base: string = 'http://ssos.backend.turpialdos.webfactional.com/';
  url_area_get: string = this.base + 'area/plant/';
  url_area_edit: string = this.base + 'area/';
  

  reqOpts = {
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token aadf212a65b598ffae2ad411775e4d1bd3f4532c',
      'Plant': '2497021f-fb0e-4dda-9728-0eee894a81a6'
      // 'APIKEY': this.apikey
    },
    params: new HttpParams()
  };

  constructor(public http: HttpClient) {
  }


  /**
   * Create local database
   */
  setDatabase(db: SQLiteObject) {
    if (this.db === null) {
      this.db = db;
    }
  }

  async createTable() {
    console.log('Creating items table');
    let sql = 'CREATE TABLE IF NOT EXISTS items(id wibblewibble primary key, name TEXT, identifier TEXT, trap_points REAL)';
    return this.db.executeSql(sql, []).then(response => {
      console.log('Table was created', JSON.stringify(response));
    }).catch(error => {
      console.error('create table error (EC0)', error.message);
      console.error('(EC0) full error', JSON.stringify(error));
    });
  }

  async dropTable(table: string) {
    console.log('Droping items table');
    let sql = 'DROP TABLE IF EXISTS' + ' ' + table;
    try {
      const response = await this.db.executeSql(sql, []);
      console.log('Table was dropped', JSON.stringify(response));
    }
    catch (error) {
      console.error('dropTable table', table, 'error (ED)', error.message);
      console.error('(ED) full error', JSON.stringify(error));
    }
  }

  downloadItem(item: any) {
    /* if record exist update it; else create it */
    const sql = 'REPLACE INTO items(id, name, identifier, trap_points) VALUES(?,?,?,?)';
    return this.db.executeSql(sql, [item.id, item.name, item.identifier, item.trap_points]);
  }

  uploadItem(item: any) {

    console.log('items values', JSON.stringify(item));

    const body = {
      'name': item.name,
      'identifier': item.price
    }

    return new Promise(resolve => {
      this.http.put(this.url_area_edit + item.id + '/', body, this.reqOpts).subscribe(data => {
        resolve(data);
      }, error => {
        console.error('Upload item error (EU0)', error.message);
        console.error('(EU0) full error', JSON.stringify(error));
      });
    });
  }

  removeItem(item: any) {
    let sql = 'DELETE FROM items WHERE id=?';
    return this.db.executeSql(sql, [item.id]);
  }

  editItem(item: any) {
    let sql = 'UPDATE items SET name=?, identifier=? WHERE id=?';
    return this.db.executeSql(sql, [item.name, item.price, item.id]);
  }

  /**
   * Get items from espacio store by its APIKEY
   */
  async getItems(connection: boolean) {

    if (connection) {
      console.log('items from server');

      // For server data
      return new Promise(resolve => {
        this.http.get(this.url_area_get, this.reqOpts).subscribe(data => {
          resolve(data);
        }, error => {
          console.error('Get items error (EG1)', error.message);
          console.error('(EG1) full error', JSON.stringify(error));
        });
      });

    } else {
      console.log('items from local');

      // For local data
      let sql = 'SELECT * FROM items';
      return this.db.executeSql(sql, []).then(response => {
        let items = [];
        for (let index = 0; index < response.rows.length; index++) {
          items.push(response.rows.item(index));
        }
        return Promise.resolve(items);
      }).catch(error => Promise.reject(error));

    }

  }

}
