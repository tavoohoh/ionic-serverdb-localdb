import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { SQLite } from '@ionic-native/sqlite';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TasksServiceProvider } from '../providers/tasks-service/tasks-service';
import { DatabaseInterfaceProvider } from '../providers/database-interface/database-interface';
import { TabsPage } from '../pages/tabs/tabs';
import { ServerDataPage } from '../pages/server-data/server-data';
import { LocalDataPage } from '../pages/local-data/local-data';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    ServerDataPage,
    LocalDataPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    ServerDataPage,
    LocalDataPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TasksServiceProvider,
    DatabaseInterfaceProvider
  ]
})
export class AppModule {}
