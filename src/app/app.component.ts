import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SQLite } from '@ionic-native/sqlite';

import { TabsPage } from '../pages/tabs/tabs';
import { DatabaseInterfaceProvider } from '../providers/database-interface/database-interface';


@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  rootPage;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public databaseInterface: DatabaseInterfaceProvider,
    public sqlite: SQLite
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.createDatabase();
    });
  }

  private createDatabase(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default' // the location field is required
    })
    .then((db) => {
      this.databaseInterface.setDatabase(db);
      return this.databaseInterface.createTable();
    })
    .then(() =>{
      this.splashScreen.hide();
      this.rootPage = TabsPage;
    })
    .catch(error =>{
      console.error(error);
    });
  }

}

