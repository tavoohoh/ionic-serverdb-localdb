import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServerDataPage } from '../server-data/server-data';
import { LocalDataPage } from '../local-data/local-data';


@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root = ServerDataPage;
  tab2Root = LocalDataPage;

  constructor() {

  }
}
