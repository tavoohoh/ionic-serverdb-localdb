import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { DatabaseInterfaceProvider } from '../../providers/database-interface/database-interface';


@IonicPage()
@Component({
  selector: 'page-local-data',
  templateUrl: 'local-data.html',
})
export class LocalDataPage {

  items: any;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public databaseInterface: DatabaseInterfaceProvider
  ) {
  }

  ionViewWillEnter() {
    this.reload();
  }

  ionViewDidLoad() {
    this.reload();
  }

  reload() {
    this.getItemsService();
  }

  getItemsService() {
    let loading = this.loadingCtrl.create({
      content: 'Loading server data...'
    });

    loading.present();

    this.databaseInterface.getItems(false).then(data => {
      this.items = data;
      loading.dismiss();
    }).catch(error => {
      loading.dismiss();
    });
  }

  removeItemFromStorage(item: any) {
    this.databaseInterface.removeItem(item).then(data => {
      let toast = this.toastCtrl.create({
        message: 'Area ' + item.name + ' was archived successfully',
        duration: 2000,
        position: 'bottom'
      });

      toast.present();
      this.reload();
    });
  }

  editItemFromStorage(item: any) {
    this.databaseInterface.editItem(item).then(response => {
      console.log('Item was edited', JSON.stringify(response));
      this.reload();
    }).catch(error => {
      console.error('Edit item error (EE1)', error.message);
      console.error('(EE1) full error', JSON.stringify(error));
    })
  }

  uploadItemToSever(item: any) {
    this.databaseInterface.uploadItem(item).then(response => {
      console.log('Item was uploaded', JSON.stringify(response));
      this.reload();

      let toast = this.toastCtrl.create({
        message: 'Area ' + item.name + ' was uploaded successfully',
        duration: 2000,
        position: 'bottom'
      });
      
      toast.present();
    }).catch(error => {
      console.error('Upload item error (EU1)', error.message);
      console.error('(EU1) full error', JSON.stringify(error));
    })
  }

  deleteAllItems() {
    return this.databaseInterface.dropTable('items');
  }

  editItemForm(item: any) {
    let alert = this.alertCtrl.create({
      title: 'Edit area',
      inputs: [
        {
          name: 'name',
          value: item.name,
          placeholder: 'Area name',
        },
        {
          name: 'identifier',
          value: item.identifier,
          placeholder: 'Area identifier',
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('canceled');
          }
        },
        {
          text: 'Save',
          handler: (data) => {
            const form = {
              'id': item.id,
              'name': data.name,
              'price': data.identifier
            };

            this.editItemFromStorage(form);
          }
        }
      ]
    });
    alert.present();
  }

}
