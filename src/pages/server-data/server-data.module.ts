import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServerDataPage } from './server-data';

@NgModule({
  declarations: [
    ServerDataPage,
  ],
  imports: [
    IonicPageModule.forChild(ServerDataPage),
  ],
})
export class ServerDataPageModule {}
