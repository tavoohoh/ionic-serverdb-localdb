import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { DatabaseInterfaceProvider } from '../../providers/database-interface/database-interface';


@IonicPage()
@Component({
  selector: 'page-server-data',
  templateUrl: 'server-data.html',
})
export class ServerDataPage {

  items: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public databaseInterface: DatabaseInterfaceProvider
  ) {
  }

  ionViewWillEnter() {
    this.reload();
  }

  ionViewDidLoad() {
    this.getItemsService();
  }

  reload() {
    this.getItemsService();
  }

  getItemsService() {
    let loading = this.loadingCtrl.create({
      content: 'Loading server data...'
    });

    loading.present();

    this.databaseInterface.getItems(true).then(data => {
      this.items = data['results'];
      loading.dismiss();
    }).catch(error => {
      loading.dismiss();
    });
  }

  downloadItem(item: any) {
    let loading = this.loadingCtrl.create({
      content: 'Downloading ' + item.name + '...'
    });
    let toast = this.toastCtrl.create({
      message: 'Area ' + item.name + ' was downloaded successfully',
      duration: 2000,
      position: 'bottom'
    });

    loading.present();

    this.databaseInterface.downloadItem(item).then(response => {
      console.log('Item was downloaded', JSON.stringify(response));
      loading.dismiss();
      toast.present();
    }).catch(error => {
      console.error('download item error (EC1)', error.message);
      console.error('(EC1) full error', JSON.stringify(error));
      loading.dismiss();
    });
  }
}
